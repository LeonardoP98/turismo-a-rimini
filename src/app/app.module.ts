import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiCallerProvider } from '../providers/api-caller/api-caller';
import { PuntiInteressePage } from '../pages/punti-interesse/punti-interesse';
import { HttpClientModule } from '@angular/common/http';
import { DetEventoPage } from '../pages/det-evento/det-evento';
import { DetAttrazionePage } from '../pages/det-attrazione/det-attrazione';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    PuntiInteressePage,
    DetEventoPage,
    DetAttrazionePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    PuntiInteressePage,
    DetEventoPage,
    DetAttrazionePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiCallerProvider
  ]
})
export class AppModule {}
