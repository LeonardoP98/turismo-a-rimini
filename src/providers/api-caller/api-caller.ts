import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';



export interface listaPost{};
export interface listaPages{};




@Injectable()
export class ApiCallerProvider {
  baseUrl='https://hawser-laid-locker.000webhostapp.com/wp-json/wp/v2';


  constructor(public http: HttpClient) {

  }

  getPosts(): Observable<listaPost[]> {
    return this.http.get<listaPost[]>(this.baseUrl+'/posts?_embed').pipe(
      map(result => {
        return result ;
      })
    );
  }


  getPages(): Observable<listaPages[]> {
    return this.http.get<listaPages[]>(this.baseUrl+'/pages?_embed').pipe(
      map(result => {
        return result ;
      })
    );
  }



}
