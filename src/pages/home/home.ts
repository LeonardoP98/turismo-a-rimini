import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiCallerProvider, listaPost } from '../../providers/api-caller/api-caller';
import { DetEventoPage } from '../det-evento/det-evento';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  posts : listaPost =null;
  allPosts :any=null;
  ricerca:string='';

  constructor(public navCtrl: NavController,
              public apiCallerProvider: ApiCallerProvider) {

    this.setEventi();

  }

  onDetEvento(p) {
    this.navCtrl.push(DetEventoPage, { post: p });
  }
  setEventi(){
    this.apiCallerProvider.getPosts().subscribe(
      lista => {
        this.posts = lista;
        this.allPosts = lista;
        // console.log(this.posts);
      }
    );
  }

  doRefresh(event) {
    this.apiCallerProvider.getPosts().subscribe(
      lista => {
        this.posts = lista;
        this.allPosts = lista;
        event.complete();
      }
    );
  }

  filtraEventi(){
    let appoggio: any[] =[];
    console.log('ricerca:',this.ricerca);
    for(let el of this.allPosts){
      console.log(el.title.rendered);
      if(el.title.rendered.toLowerCase().indexOf(this.ricerca.toLowerCase()) > -1){
        appoggio.push(el);
      }
    }
    this.posts = appoggio;
  }
}
