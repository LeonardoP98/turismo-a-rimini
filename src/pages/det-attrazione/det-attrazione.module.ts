import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetAttrazionePage } from './det-attrazione';

@NgModule({
  declarations: [
    DetAttrazionePage,
  ],
  imports: [
    IonicPageModule.forChild(DetAttrazionePage),
  ],
})
export class DetAttrazionePageModule {}
