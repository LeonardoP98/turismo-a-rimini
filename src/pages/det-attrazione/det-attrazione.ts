import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the DetAttrazionePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-det-attrazione',
  templateUrl: 'det-attrazione.html',
})
export class DetAttrazionePage {
  singlePages:any[]=[];
  contenuto:any=[null];
  iframeUrl: any=null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    sanitizer:DomSanitizer) {
    this.singlePages=this.navParams.get("page");
    // console.log(this.singlePages);
    let iframe = this.singlePages['content']['rendered'];
    iframe= iframe.match(/<iframe([\w\W]+?)>/);
    console.log(iframe[1]);
    this.contenuto=this.singlePages['content']['rendered'].replace('width', 'none').replace('height', 'none');
    this.iframeUrl=sanitizer.bypassSecurityTrustResourceUrl(this.getUrl(iframe[1]));
    console.log(this.iframeUrl);
  }

  getUrl(str){
    var res= str.split('"');
    console.log(res);
    for (let el of res){
      if(el.indexOf("http")>-1){
        return el;
      }
    }
    return false;
  }
}
