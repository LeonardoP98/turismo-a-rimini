import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider, listaPages } from '../../providers/api-caller/api-caller';
import { DetAttrazionePage } from '../det-attrazione/det-attrazione';




@IonicPage()
@Component({
  selector: 'page-punti-interesse',
  templateUrl: 'punti-interesse.html',
})
export class PuntiInteressePage {
  pages:listaPages=null;
  allPages:any=null;
  ricerca:string='';


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public apiCallerProvider: ApiCallerProvider) {

    this.setPuntiDiInteresse();
  }

  ionViewDidLoad() {
  }

  setPuntiDiInteresse(){
    this.apiCallerProvider.getPages().subscribe(
      lista => {
        let appoggio:any[]=[];
        for (let el of lista){
          // console.log(el);
          if(el['tags'] == 5){
            (appoggio).push(el);
          }
        }
        this.pages = appoggio;
        this.allPages = appoggio;
        // console.log(this.pages);
      }
    );
  }

  onDetAttrazione(p) {
    this.navCtrl.push(DetAttrazionePage, { page: p });
  }

  doRefresh(event) {
    this.apiCallerProvider.getPages().subscribe(
      lista => {
        let appoggio:any[]=[];
        for (let el of lista){
          // console.log(el);
          if(el['tags'] == 5){
            (appoggio).push(el);
          }
        }
        this.pages = appoggio;
        this.allPages = appoggio;
        // console.log(this.pages);
        event.complete();
      }
    );
  }

  filtraAttrazioni(){
    let appoggio: any[] =[];
    console.log('ricerca:',this.ricerca);
    for(let el of this.allPages){
      console.log(el.title.rendered);
      if(el.title.rendered.toLowerCase().indexOf(this.ricerca.toLowerCase()) > -1){
        appoggio.push(el);
      }
    }
    this.pages = appoggio;
  }
}
