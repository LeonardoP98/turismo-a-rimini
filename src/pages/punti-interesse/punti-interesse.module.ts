import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PuntiInteressePage } from './punti-interesse';

@NgModule({
  declarations: [
    PuntiInteressePage,
  ],
  imports: [
    IonicPageModule.forChild(PuntiInteressePage),
  ],
})
export class PuntiInteressePageModule {}
