import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetEventoPage } from './det-evento';

@NgModule({
  declarations: [
    DetEventoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetEventoPage),
  ],
})
export class DetEventoPageModule {}
