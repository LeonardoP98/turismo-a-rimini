import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-det-evento',
  templateUrl: 'det-evento.html',
})
export class DetEventoPage {
  singlePost:any[]=[];
  contenuto:any=[null];

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this.singlePost=this.navParams.get("post");
    // console.log(this.singlePost);
    this.contenuto=this.singlePost['content']['rendered'].replace(/<img[^>]*>/g,"");
  }

  ionViewDidLoad() {}

}
